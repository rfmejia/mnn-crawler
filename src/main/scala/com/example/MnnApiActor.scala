package com.example

import akka.actor.{Actor, ActorLogging, Props}
import akka.http.scaladsl.client.RequestBuilding._
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.{HttpEntity, HttpRequest, HttpResponse, IllegalResponseException, MediaType, Uri}
import akka.http.scaladsl.model.ContentTypes.`application/json`
import akka.http.scaladsl.model.headers.OAuth2BearerToken
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.pattern.ask
import akka.stream.ActorFlowMaterializer
import akka.util.Timeout
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Success, Failure}
import spray.json._

class MnnApiActor extends Actor with ActorLogging with SprayJsonSupport {
  import MnnApiActor._

  implicit val materializer = ActorFlowMaterializer()

  import context.dispatcher

  val mnnTokenActor = context.actorOf(MnnTokenActor.props, "mnnTokenActor")

  var uploadUri: Uri = Uri.Empty
  var queryBody: JsObject = JsObject.empty
  var jwt: AuthToken = AuthToken.Empty

  def receive = {
    case Initialize(uri, authInfo) =>
      uploadUri = uri

      log.info(s"""Configuration:
      URL: ${uploadUri}
      """)

      // ask for token, then send to sender
      val replyTo = sender()
      implicit val timeout = Timeout(1 minute)

      val request = (mnnTokenActor ? AcquireToken(authInfo)) 
      
      request onComplete {
        case Success(t: MnnToken) => 
          jwt = t
          replyTo ! MasterActor.Ready("MnnApiActor.Initialize")
        case Success(unknown: Any) =>
          replyTo ! MasterActor.Halt("Unexpected response from MnnTokenActor", 
            IllegalResponseException(unknown.toString))
        case Failure(err) =>
          replyTo ! MasterActor.Halt("Could not request token", err)
      }
    
    case SaveResources(resources) =>
      import context.system

      val pipeline: RequestTransformer = {
        addCredentials(OAuth2BearerToken(jwt.value))
      }
      val requests: Seq[HttpRequest] = resources
        .map { case (body, mediaType) => HttpEntity(mediaType, body) }
        .map(entity => pipeline(Post(uploadUri, entity)))

      val responses: Future[Seq[HttpResponse]] = 
        requests.foldLeft(Future.successful(List.empty[HttpResponse])) {
          (b, a) => for {
            list <- b
            resp <- Http().singleRequest(a)
          } yield list :+ resp
        }

      val replyTo = sender()
      responses onComplete {
        case Success(res) => replyTo ! ApiResponses(res)
        case Failure(err) => replyTo ! MasterActor.ErrorLog("Could not save resource to mnn", err)
      }
  }
}

object MnnApiActor {
  val props = Props[MnnApiActor]

  case class Initialize(uploadUri: Uri, authInfo: AuthInfo)
  case class SaveResources(resources: Seq[(String, MediaType)])

  case class ApiResponses(responses: Seq[HttpResponse])
}

