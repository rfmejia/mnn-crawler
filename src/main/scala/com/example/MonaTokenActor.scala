package com.example

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.http.scaladsl.client.RequestBuilding._
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.{HttpEntity, HttpRequest, IllegalResponseException, Uri, StatusCodes}
import akka.http.scaladsl.model.ContentTypes.`application/json`
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorFlowMaterializer
import scala.util.{Success, Failure}
import spray.json.DefaultJsonProtocol

class MonaTokenActor extends Actor with ActorLogging with SprayJsonSupport with MonaTokenProtocols {
  import MonaTokenActor._

  implicit val materializer = ActorFlowMaterializer()

  def receive = {
    case AcquireToken(info) =>
      import context.dispatcher
      import context.system

      val authBody = s"""
      {
        "email": "${info.username}",
        "password": "${info.password}"
      }
      """
      
      val request = Post(info.loginUrl, HttpEntity(`application/json`, authBody))
      val um = sprayJsonUnmarshaller[MonaToken](tokenFormat, context.dispatcher, materializer)

      val response = for {
        res <- Http().singleRequest(request)
        tok <- Unmarshal(res.entity).to[MonaToken]
      } yield tok
      
      val replyTo = sender()
      response onComplete {
        case Success(token) => 
          replyTo ! token
        case Failure(err) => 
          replyTo ! MasterActor.Halt(Some("Could not parse auth response"), Some(err))
      }
  }
}

object MonaTokenActor {
  val props = Props[MonaTokenActor]
}

trait MonaTokenProtocols extends DefaultJsonProtocol {
  implicit val tokenFormat = jsonFormat(MonaToken.apply, "id", "emailAddress", "access_token", "firstName", "lastName", "institution")
}

