package com.example

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.http.scaladsl.client.RequestBuilding._
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpResponse, IllegalResponseException, Uri, StatusCodes}
import akka.http.scaladsl.model.headers.BasicHttpCredentials
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorFlowMaterializer
import scala.concurrent.Future
import scala.util.{Success, Failure}
import spray.json.DefaultJsonProtocol

class MnnTokenActor extends Actor with ActorLogging with SprayJsonSupport with MnnTokenProtocols {
  import MnnTokenActor._

  implicit val materializer = ActorFlowMaterializer()

  def receive = {
    case AcquireToken(info) =>
      import context.dispatcher
      import context.system

      val pipeline: RequestTransformer = {
        addCredentials(new BasicHttpCredentials(info.username, info.password))
      }
      
      val request = pipeline(Post(info.loginUrl))
      log.info(s"Issuing request $info")

      val um = sprayJsonUnmarshaller[MnnToken](tokenFormat, context.dispatcher, materializer)
      val response = for {
        res <- Http().singleRequest(request)
        at  <- Unmarshal(res.entity).to[MnnToken]
      } yield at

      val replyTo = sender()
      response onComplete {
        case Success(token) => 
          log.info(s"Acquired mnn JWT: $token")
          replyTo ! token
        case Failure(err) =>
          log.error(s"ERROR: $err")
          replyTo ! err
      }
  }
}

object MnnTokenActor {
  val props = Props[MnnTokenActor]
}

trait MnnTokenProtocols extends DefaultJsonProtocol {
  implicit val tokenFormat = jsonFormat(MnnToken.apply, "token", "exp")
}

