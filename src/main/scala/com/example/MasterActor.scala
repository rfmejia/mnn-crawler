package com.example

import akka.actor.{Actor, ActorLogging, Props}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.{MediaType, StatusCodes, Uri}
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.config.{Config, ConfigRenderOptions}
import scala.collection.mutable
import scala.concurrent.duration._
import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.{Try, Success, Failure}
import spray.json._

class MasterActor extends Actor with ActorLogging with SprayJsonSupport {
  import MasterActor._

  val dependencies = mutable.Set("MonaApiActor.Initialize", "MnnApiActor.Initialize")
  log.info(s"Initialization dependencies: ${dependencies}")

  val monaApiActor = context.actorOf(MonaApiActor.props, "monaApiActor")
  val mnnApiActor = context.actorOf(MnnApiActor.props, "mnnApiActor")

  val `application/vnd.mona.individual` = MediaType.custom("application/vnd.mona.individual", MediaType.Encoding.Open)

  var totalQueried = 0
  var totalSaved = 0
  var totalFailed = 0
  var monaEndOfPages = false
  
  implicit val timeout = Timeout(1 minute)

  def receive = {
    case Initialize(c: Config) =>
      // Required configuration parameters for MoNA
      Try {
        val searchUrl = Uri(c.getString("mona.search.url"))
        val offset = c.getInt("mona.search.offset")
        val defaultMax = c.getInt("mona.search.max")
        val renderOptions = ConfigRenderOptions.concise().setJson(true).setFormatted(true)
        val queryBody = c.getConfig("mona.search.query").resolve().root.render(renderOptions).parseJson.asJsObject
        val ai = AuthInfo(c.getString("mona.login.url"), c.getString("mona.login.email"), c.getString("mona.login.password"))

        (ai, searchUrl, offset, defaultMax, queryBody)
      } match {
        case Success((authInfo, searchUrl, offset, defaultMax, queryBody)) => // Initialize dependencies
          monaApiActor ! MonaApiActor.Initialize(searchUrl, offset, defaultMax, queryBody)
        case Failure(err) => self ! Halt(Some("Cannot boot system"), Some(err))
      }

      // Required configuration parameters for mnn-data-node
      Try {
        val uploadUrl = c.getString("mnn.upload.url")
        val ai = AuthInfo(c.getString("mnn.login.url"), c.getString("mnn.login.username"), c.getString("mnn.login.password"))
        (uploadUrl, ai)
      } match {
        case Success((uploadUrl, authInfo)) => // Initialize dependencies
          mnnApiActor ! MnnApiActor.Initialize(uploadUrl, authInfo)
        case Failure(err) => self ! Halt(Some("Cannot boot system"), Some(err))
      }

    case Ready(src: String) => 
      log.info(s"'${src}' is ready.")
      dependencies.remove(src)
      if (dependencies.isEmpty) self ! Start

    case Start =>
      log.info("Starting...")
      monaApiActor ! MonaApiActor.RequestNextPage

    case MonaApiActor.NextPage(items) =>
      import context.dispatcher
      totalQueried = totalQueried + items.elements.length
      val entities = items.elements.map(_.toString) zip
        List.fill(items.elements.length)(`application/vnd.mona.individual`) 
      mnnApiActor ! MnnApiActor.SaveResources(entities)
      
    case MnnApiActor.ApiResponses(responses) =>
      responses.map(_.status) map {
        case StatusCodes.Success(_) => totalSaved = totalSaved + 1
        case _ => totalFailed = totalFailed + 1
      }
      log.info(s"Queried: $totalQueried ; Saved: $totalSaved ; Failed: $totalFailed")

      if(!monaEndOfPages) monaApiActor ! MonaApiActor.RequestNextPage

    case MonaApiActor.EndOfPages => monaEndOfPages = true

    case ErrorLog(msg, err) =>
      log.error(msg)
      log.error(err.getMessage)

    case Halt(msg, err) =>
      msg foreach log.info
      err map(_.getMessage) foreach log.info
      log.info("halt")
  	  context.system.shutdown()
  }
}

object MasterActor {
  val props = Props[MasterActor]

  case class Initialize(c: Config)
  case class Ready(src: String)
  case object Start
  case class ErrorLog(msg: String, err: Throwable)
  case class Halt(msg: Option[String] = None, err: Option[Throwable] = None)
  object Halt {
    def apply(msg: String, err: Throwable): Halt = Halt(Option(msg), Option(err))
    def apply(msg: String): Halt = Halt(Option(msg), None)
    def apply(err: Throwable): Halt = Halt(None, Option(err))
  }
}

