package com.example

sealed trait AuthToken {
  val value: String
}
object AuthToken {
  val Empty = EmptyToken
}

final case object EmptyToken extends AuthToken {
  val value = "(empty)"
}
final case class MonaToken(id: String, emailAddress: String, access_token: String, firstName: String, lastName: String, institution: String) extends AuthToken {
  val value = access_token
}
final case class MnnToken(token: String, exp: String) extends AuthToken {
  val value = token
}

case class AuthInfo(loginUrl: String, username: String, password: String)
case class AcquireToken(info: AuthInfo)

