package com.example

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.http.scaladsl.client.RequestBuilding._
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.{HttpEntity, HttpRequest, IllegalResponseException, StatusCodes, Uri}
import akka.http.scaladsl.model.ContentTypes.`application/json`
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorFlowMaterializer
import akka.util.Timeout
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Success, Failure}
import spray.json._

class MonaApiActor extends Actor with ActorLogging with SprayJsonSupport {
  import MonaApiActor._

  implicit val materializer = ActorFlowMaterializer()

  var searchUri: Uri = Uri.Empty
  var defaultMax: Int = 10
  var queryBody: JsObject = JsObject.empty
  var offset: Int = 0

  implicit val timeout = Timeout(1 minute)

  def receive = {
    case Initialize(uri, off, max, json) =>
      searchUri = uri
      offset = off
      defaultMax = max
      queryBody = json
      sender ! MasterActor.Ready("MonaApiActor.Initialize")
    
    case RequestNextPage =>
      import context.dispatcher
      import context.system

      val nextUri: Uri = searchUri.withQuery("offset" -> offset.toString, "max" -> defaultMax.toString)
      queryBody = JsObject(queryBody.fields + ( ("offset", JsNumber(offset)) ))
      offset = offset + defaultMax
      
      val pipeline = {}

      val request = Post(nextUri, HttpEntity(`application/json`, queryBody.toString))

      println(s"MoNA => uri: $nextUri, query $queryBody")
      val replyTo = sender()
      Http().singleRequest(request) onComplete {
        case Success(response) =>
          response.status match {
            case StatusCodes.OK =>
              for (items <- Unmarshal(response.entity).to[JsValue]) {
                items match {
                  case arr: JsArray =>
                    replyTo ! NextPage(arr)
                  case unknown: JsValue => 
                    replyTo ! MasterActor.Halt("Unexpected response from MoNA API (must be a JSON array)", 
                      IllegalResponseException(unknown.toString))
                }
              }
            case StatusCodes.NotFound =>
              replyTo ! EndOfPages
            case _ =>
              replyTo ! MasterActor.ErrorLog(s"Error ${response.status}", new IllegalStateException(response.toString))
          }
        case Failure(err) =>
          log.warning(s"Error fetching ${nextUri}")
          log.warning(err.getMessage())
      }
  }
}

object MonaApiActor {
  val props = Props[MonaApiActor]

  case class Initialize(defaultUri: Uri, offset: Int, defaultMax: Int, queryBody: JsObject)
  case object RequestNextPage
  case object EndOfPages

  case class NextPage(items: JsArray)
}

