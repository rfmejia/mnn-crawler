package com.example

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory

object ApplicationMain extends App {
  val system = ActorSystem("MyActorSystem")
  val config = ConfigFactory.load()

  val masterActor = system.actorOf(MasterActor.props, "masterActor")
  masterActor ! MasterActor.Initialize(config)

  system.awaitTermination()
}

