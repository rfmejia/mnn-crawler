name := """minimal-akka-scala-seed"""

version := "1.0"

scalaVersion := "2.11.6"

scalacOptions := Seq(
  "-Xlint",
  "-Xfatal-warnings",
  "-deprecation",
  "-feature",
  "-unchecked",
  "-encoding", "utf8")

libraryDependencies ++= {
  val akkaV       = "2.3.11"
  val akkaStreamV = "1.0-RC2"
  val scalaTestV  = "2.2.4"
  Seq(
    "com.typesafe.akka" %% "akka-actor"                           % akkaV,
    "com.typesafe.akka" %% "akka-stream-experimental"             % akkaStreamV,
    "com.typesafe.akka" %% "akka-http-core-experimental"          % akkaStreamV,
    "com.typesafe.akka" %% "akka-http-scala-experimental"         % akkaStreamV,
    "com.typesafe.akka" %% "akka-http-spray-json-experimental"    % akkaStreamV,
    "com.typesafe.akka" %% "akka-http-testkit-scala-experimental" % akkaStreamV,
    "io.spray"          %% "spray-json"                           % "1.3.2",
    "org.scalatest"     %% "scalatest"                            % scalaTestV % "test"
  )
}

enablePlugins(JavaAppPackaging)

Revolver.settings
