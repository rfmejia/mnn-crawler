## Fields needed from MoNA

- compound name*
- precursor mz*
- precursor type
- retention time
- inchi key
- formula
- ion mode
- ms/ms spectrum*
* required

where

     ms type == ms2
     ion mode == "positive" or "negative"


## MoNA search queries

Issue a POST request to [http://mona.fiehnlab.ucdavis.edu/rest/spectra/search] with the following JSON body template:

    {
      "biologicalCompound": {},
      "chemicalCompound": {},
      "metadata": {},
      "tags": []
    }

URL parameters are:

    offset=[Integer]&max=[Integer]

| Name    | Description |
| ------- | ----------- |
| eq      | The value must be identical |
| like    | a case sensitive text matching query |
| ilike   | a none case sensitive text matching query |
| gt      | the value is greater than |
| lt      | the value is less than |
| ge      | the value is greater or equals |
| less    | the value is less or equals |
| ne      | the value doesn't equal |
| between | the value is in between |
| in      | the value is in a list of options |

## MoNA login

Issue a POST request to [http://mona.fiehnlab.ucdavis.edu/rest/login] with the following JSON body

    {
      "email": "myemail@gmail.com",
      "password": "mypassword"
    }

Forward the access token to subsequent requests using the `X-Auth-Token` HTTP header.




val p: Policies = (Selection, Re-visit, Politeness, Parallelization)

trait Seeder {
  def generate(c: Config, p: Option[Policies]): List[URL]
}

val s: Set[Seeder] = from config

# Crawler module

f(seeders, p) -> (URL stream, p')

*f may be a composition of one or more processes (seeders, frontier)

#
